import { getCardTemplate } from './templates/get-card-template';
import { fetchCharactersData } from './fetch-characters-data';

export const displayCharactersData = (rootElement) => {
  let queries = {
    limit: 15,
    page: 0,
    nameStartsWith: '',
  };

  const setCharactersQueries = (nextQueries) => {
    queries = { ...queries, ...nextQueries };
  };

  const resetPage = () => {
    queries.page = 0;
    rootElement.innerHTML = '';
  };

  const loadCharacters = () => {
    fetchCharactersData(queries).then((response) => {
      const html = response.data.results
        .map(({ id, name, description, thumbnail }) =>
          getCardTemplate({
            id,
            name,
            description,
            thumbnail: `${thumbnail.path}.${thumbnail.extension}`,
          })
        )
        .join('');

      rootElement.insertAdjacentHTML('beforeend', html);
      queries.page += 1;
    });
  };

  return {
    setCharactersQueries,
    loadCharacters,
    resetPage,
  };
};
